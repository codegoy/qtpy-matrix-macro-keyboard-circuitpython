from adafruit_hid.keyboard_layout_us import KeyboardLayoutUS
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode
from digitalio import DigitalInOut
import adafruit_matrixkeypad
import neopixel
import usb_hid
import board

pixel = neopixel.NeoPixel(board.NEOPIXEL, 10, auto_write=False)
pixel_off = (0, 0, 0)
pixel_red = (200, 0, 0)
pixel_green = (0, 200, 0)
pixel_blue = (0, 0, 200)
pixel_direction = "up"
pixel_brightness = 10
pixel_brightness_max = pixel_brightness
keyboard = Keyboard(usb_hid.devices)
keyboard_layout = KeyboardLayoutUS(keyboard)
c = [DigitalInOut(x) for x in (board.D0,
                               board.D1,
                               board.D2,
                               board.D3,
                               board.D10)]
r = [DigitalInOut(x) for x in (board.D6,
                               board.D12)]
k = ((1, 2, 3, 4, 5),
     (6, 7, 8, 9, 10))
keypad = adafruit_matrixkeypad.Matrix_Keypad(r, c, k)


def handle_button_press(button_input):
    for x in button_input:
        if x == 1:
            return True
        elif x == 2:
            return True
        elif x == 3:
            return True
        elif x == 4:
            return True
        elif x == 5:
            print("PASTE")
            keyboard.send(Keycode.CONTROL, Keycode.V)
        elif x == 6:
            print("shift")
            keyboard.press(Keycode.SHIFT)
        elif x == 7:
            return True
        elif x == 8:
            return True
        elif x == 9:
            return True
        elif x == 10:
            return True


def all_pixels_off():
    for x in range(0, 10):
        pixel[x] = (0, 0, 0)
    pixel.show()


def pixel_pulse(color, pixels):
    global pixel_brightness, pixel_brightness_max, pixel_direction
    pixel_action = (pixel_brightness, 0, 0)
    if pixel_brightness == 0:
        pixel_direction = "up"
    elif pixel_brightness == pixel_brightness_max:
        pixel_direction = "down"
    if pixel_direction == "up":
        pixel_brightness = pixel_brightness + 1
    else:
        pixel_brightness = pixel_brightness - 1
    if color == "red":
        pixel_action = (pixel_brightness, 0, 0)
    elif color == "green":
        pixel_action = (0, pixel_brightness, 0)
    elif color == "blue":
        pixel_action = (0, 0, pixel_brightness)
    if pixels == "all":
        for x in range(0, 10):
            pixel[x] = pixel_action
    else:
        pixel[pixels] = pixel_action
    pixel.show()


while True:
    pixel_pulse("green", "all")
    pk = keypad.pressed_keys
    if pk:
        all_pixels_off()
        print(pk)
        handle_button_press(pk)
        # block until released
        while len(pk) > 0:
            pixel_pulse("blue", pk[0]-1)
            pk = keypad.pressed_keys
    keyboard.release_all()
